import React from "react";

const Nav = () => {
  return (
    <header className="header">
      <div className="container">
        <div className="row_flex">
          <div className="logo_section">
            <a href="index.php" className="rem_link_style">
              <img src="/img/logo.png" alt="" className="img-responsive" />
            </a>
          </div>
          <div className="link_section">
            <a href="work_listing.php">Work</a>
            <a href="services.php">Services</a>
            <a href="career.php">Careers</a>
            <a href="blog.php">Ting Things</a>
            <a href="contact.php">Contact</a>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Nav;
