import React from "react";

const Fotter = () => {
  return (
    <div className="fotter">
      <footer class="black_bg">
        <div class="container footer_inner">
          <a href="index.php" class="each_fi">
            <img src="img/logo.png" alt="Logo" class="footer_logo" />
          </a>
          <div class="each_fi">
            <div class="footer_links">
              <a href="work_listing.php" class="small_title">
                Our Work
              </a>
              <a href="services.php" class="small_title">
                Services
              </a>
              <a href="javascript:void(0)" class="small_title">
                About Us
              </a>
            </div>
          </div>
          <div class="each_fi">
            <div class="footer_links">
              <a href="blog.php" class="small_title">
                What's new
              </a>
              <a href="contact.php" class="small_title">
                Contact Us
              </a>
            </div>
          </div>
          <div class="each_fi">
            <div class="footer_links">
              <a href="javascript:void(0)" class="small_title">
                Reach out
              </a>
            </div>
            <a href="mailto:careers@ting.in" class="ting_mail">
              careers@ting.in
            </a>
            <div class="social_links">
              <p class="social_icons">
                <a
                  class="rem_link_style facebook"
                  href="https://www.facebook.com/TINGAdvertising/?ref=hl"
                  target="_blank"
                >
                  <img src="img/facebook.svg" alt="facebook" />
                </a>
                <a
                  class="rem_link_style linkedin"
                  href="https://www.linkedin.com/uas/login?session_redirect=https%3A%2F%2Fwww.linkedin.com%2Fcompany%2F616876%2Fadmin"
                  target="_blank"
                >
                  <img src="img/linkedin.svg" alt="linkedin" />
                </a>
                <a
                  class="rem_link_style instagram"
                  href="https://www.instagram.com/tinglabs/"
                  target="_blank"
                >
                  <img src="img/instagram.svg" alt="instagram" />
                </a>
                <a
                  class="rem_link_style youtube"
                  href="https://www.youtube.com/channel/UCN5bKktCED7StjmnX4tmJuQ/videos?view=0&sort=dd&view_as=subscriber"
                  target="_blank"
                >
                  <img src="img/youtube.svg" alt="youtube" />
                </a>
              </p>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
};

export default Fotter;
