import React, { useState } from 'react'
import { Nav, Fotter } from "../components"
import { request, gql } from 'graphql-request'

const contact = ({ data }) => {
    const [select, setSelect] = useState(0)


    console.log(data)
    return (
        <>
            <Nav />
            <main id="contact_page" className="smooth_scroll_pages">
                <div className="mt_60"></div>
                <div className="container">
                    <div className="paper_effect"></div>
                    <div className="paper_effect"></div>
                </div>
                <div className="cmn_heads">
                    <div className="container">
                        <div className="bg_white">
                            <p className="main_title">ting Daily</p>
                            <div className="border_light"></div>
                            <div className="section_small_title">
                                <p className="small_title">Contact Us</p>
                            </div>
                            <div className="border_dark"></div>
                        </div>
                    </div>
                </div>

                <section className="tab_wrap">
                    <div className="container">
                        <div className="tab_wrap_inner bg_white">
                            <h4 className="mainbig_title">Lorem Ipsum...!</h4>
                            <div className="tab_buttons">
                                {
                                    data && data.map((curElem, id) => (
                                        <div className={select == id ? "tab_button_item active" : "tab_button_item"} onClick={() => setSelect(id)}>
                                            <p>{curElem.name}</p>
                                        </div>
                                    ))
                                }

                            </div>
                            {
                                data && data.map((curElem, id) => (
                                    <div className="tab_content">
                                        <div className={select == id ? "tab_content_item active" : "tab_content_item"}>
                                            <div className="addMap_Wrap">
                                                <div className="contact_address">
                                                    <div className="branch_inner">
                                                        <div className="cb_inner">
                                                            <p className="small_title">Address:</p>
                                                            <p className="branch_add cmn_branch_cont">{curElem.addess}</p>
                                                            <div className="phones">
                                                                <p className="small_title">Business Enquiries:</p>
                                                                <a href="tel:+91 98675 97223" className="cmn_branch_cont">+91 {curElem.buisnessPhone1}</a>
                                                                <a href="tel:+91 98202 82644" className="cmn_branch_cont">+91 {curElem.buisnessPhone2}</a>
                                                            </div>
                                                            <div className="phones">
                                                                <p className="small_title">Job Openings:</p>
                                                                <a href="tel:+91 93210 69106" className="cmn_branch_cont">+91 {curElem.jobNumber}</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="map_location">
                                                    <iframe src={curElem.maplink} width="100%" height="300" ></iframe>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                ))
                            }
                        </div>
                        <div className="below_branch bg_white">
                            <p className="bb_content">
                                These locations mattered before 2020, post 2020, we're closer to you than ever before. <br />
                                Few office addresses here, but TBH with you, how does it matter, we're one click and a video
                                call away.
                            </p>
                            <div className="link_btn">
                                <a href="https://ting.kissflow.com/public/Pf4961c365-0b6c-4453-83fb-1d6057e39f4a" className="rem_link_style cc_btn" target="_blank">
                                    Reach out <img src="img/right-arrow.png" alt="arrows" className="cmnYarrow" />
                                </a>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
            <Fotter />
        </>
    )
}

export default contact


export async function getServerSideProps() {
    const query = gql`
    {
        contacts {
    name
    addess
    maplink
    buisnessPhone1
    buisnessPhone2
    jobNumber
  }
    }`
    const data = await request("https://api-ap-south-1.graphcms.com/v2/cl2a6rrly1spu01yw0k61038m/master", query)
    return {
        props: {
            data: data.contacts
        }
    }
}

