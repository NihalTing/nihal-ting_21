import '../styles/globals.scss'
import '../styles/Nav.scss'
import '../styles/Contact.scss'
import '../styles/Fotter.scss'


function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
